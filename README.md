# OS Mockup Kit GitLab Repository

The OS Mockup Kit is the biggest resource kit that you can find to make mockups of any kind!

It is currently maintained by a group of people, instead of a singular person, to make it the best resource pack you can find.

# Installation:
You download one of parts of the pack anywhere, and you unpack it anywhere. That's it!

# FAQ
_What is OSMK?_
It has been answered already.

_Who originally created the kit?_
It goes as far back as fean's WNR kit, from 2017. Twiglets1888 has modernized the pack into WM Kit. It has been abandoned by Twiglets1888 and has been in hands of Glosswired (fusoxide) and has been re-named to OS Mockup Kit. It has been "abandoned" by her many times, until finally abandoning the project with release of 1.2 in 2018. There were unsuccessful attempts of reviving by several members, including current project leader (and formerly website hoster for OSMK) and other people from the group.

_When is *version* going to be out?_
We don't know, and we don't know when we'll let you know when it'll be out. Just be patient.

_Can I join the group?_
Currently, yes! Although we have strict rules when it comes to "developing" the kit, we're open for new people!

_Why is *insert file name here* not here?_
Either it's not that much required, or we just haven't added it.

_How are you going to update it?_
Simple! We'll have base version after every verion released (which will replace previous one), and a patch for ones who have previous version, which will be kept, depending on their size.
